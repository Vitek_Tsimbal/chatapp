const expect = require('expect')
const {isRealString} = require('./validation.js')

describe('isRealString function', () => {
  it('should reject non-string value', () => {
    var res = isRealString(98)

    expect(res).toBe(false)
  })

  it('should reject string with only spaces', () => {
    var res = isRealString('    ')

    expect(res).toBe(false)
  })

  it('should allow string with non-space chars', () => {
    var res = isRealString('123')

    expect(res).toBe(true)
  })
})
