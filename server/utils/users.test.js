const expect = require('expect')

const {Users} = require('./users.js')

describe('Users', () => {
  var users
  
  beforeEach(() => {
    users = new Users()

    users.users = [{
      id: '1',
      name: '123',
      room: 'NodeCourse'
    },
    {
      id: '2',
      name: '22123',
      room: 'NodeCourse'
    },
    {
      id: '3',
      name: '12123',
      room: 'NodeCourse_1'
    }]
  })

  it('should add new user', () => {
    var users = new Users()
    var user = {
      id: 123,
      name: 'Vitek',
      room: 'Room1'
    }

    var responseUser = users.addUser(user.id, user.name, user.room)

    expect(users.users).toEqual([responseUser])
  })


  it('should return names for node course', () => {
    var userList = users.getUserList('NodeCourse')

    expect(userList).toEqual(['123', '22123'])
  })

  it('should remove a user', () => {
    var userId = '1'
    var user = users.removeUser(userId)

    expect(user.id).toBe(userId)
  })

  it('should not remove user', () => {
    var userId = '14321'
    var user = users.removeUser(userId)

    expect(user).toNotExist(userId)
  })

  it('should find user', () => {
    var userId = '1'
    var user = users.getUser(userId)

    expect(user.id).toBe(userId)
  })

  it('should not find a user', () => {
    var userId = '221421412'
    var user = users.getUser(userId)

    expect(user).toBe(undefined)
  })

  it('should return names for node course', () => {
    var userList = users.getUserList('NodeCourse_1')

    expect(userList).toEqual(['12123'])
  })
})